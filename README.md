# Italk
<img src="https://gitlab.com/lhdof.hef3/italk/-/wikis/uploads/72841993538a4158572cd2d8be11527d/logo.jpg" 
    width="400"
    alt="Logo do projeto"/>
# Autores

Edmo Oliveira - endoj.hef3@uea.edu.br

Leandro Bezerra - ldsb.hef3@uea.edu.br

Lucas Hipérides - lhdof.hef3@uea.edu.br

Priscila Gonçalves - pldsg.hef3@uea.edu.br
# Resumo
O Italk tem como proposta principal realizar o ensino de Libras de uma forma inovadora.

Combinando mecanismos de reconhecimento de imagens com aplicações interativas, unidas à uma interface simples e com jogos educacionais.

Mesmo com o foco no público infantil, qualquer pessoa pode utilizá-lo devido a sua simplicidade.

